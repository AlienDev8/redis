const redis = require("redis");
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();

const portRedis = process.envPORT || 6379;
const clientRedis = new redis.createClient();
clientRedis.on('connect', function(c){ console.log("Conectado a redis!");});
clientRedis.on('ready', function (c) { console.log("Redis listo!"); });
clientRedis.on('reconnecting', function (c) { console.log("Reconectado con redis!"); });
clientRedis.on('end', function (c) { console.log("Redis detenido!"); });
clientRedis.on("error", function(e){console.log(e);});


app.use(cors());
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json())

app.get("/set", function(req, res){
    clientRedis.set("user","{'nombre':'Roberto valles','id':100}");
    res.send("Datos guardados en redis");
    res.end();
})
app.get("/change/:username", function (req, res) {    
    clientRedis.set("user", req.params.username);
    res.send("Datos actualizados reemplazados en redis");
    res.end();
})
app.get("/clearcache", function(req, res){
    clientRedis.flushdb(function(err, succeed){
        if(succeed){
            res.send("Cache de redis limpia!!");
            res.end();
        }
    })
})


app.get("/", function(req, res){
    clientRedis.get("user", function(err, data){
         if(err !=null) { console.log(err);}        
        res.send(data);
        res.end();
    });    
})


app.listen(4000, function(){
    console.log("Server corriendo en el puerto 3000");
});